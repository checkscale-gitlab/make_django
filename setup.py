import setuptools
import os

HERE = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(HERE, "README.md"), "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="microservice_maker",
    version=os.environ["CI_COMMIT_TAG"],
    author="Victor Coelho",
    author_email="victorhdcoelho@gmail.com",
    description="Lib to create python microservices with django and flask" ,
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/victorhdcoelho/make_django",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    scripts=['bin/make-microservice'],
    dependency_links=[
        "django @ git+https://github.com/victorhdcoelho/django@main"
    ],
    install_requires=["PySimpleGUI", "tqdm"],
    include_package_data=True,
    zip_safe=False
)
